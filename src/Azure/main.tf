terraform {
  required_providers {
    azurerm = "= 1.36"
  }
  backend "azurerm" {}
}

module "hosting" {
  source = "./modules/hosting" 
}

module "database" {
  source = "./modules/database"
}