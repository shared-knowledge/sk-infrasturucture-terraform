
## =================================  !!!!!!!!!!!!!!!!!!! DO NOT CHANGE VARIABLES  !!!!!!!!!!!!!!!!!!!!!!!!!! ========================== ##

## Changing variables here or on octopus may cause resource deletion and re-creation. 

## ========================================================== General Variables ======================================================== ##
variable "environment" {
    default = "#{Environment}"
}

variable "resource_location" {
    default = "#{ResourceLocation}"
}

## ======================================================== MYSQL Server Variables ==================================================== ##

variable "sql_server_admin_password" {
  default = "#{SqlServer.Admin.Password}"
}

## ===================================================== SQL Server Firewall Variables ================================================ ##

variable "octopus_jumpbox_ip_start" {
  default = "#{Octopus.JumpBox.IPAddress.Start}"
}

variable "octopus_jumpbox_ip_end" {
  default = "#{Octopus.JumpBox.IPAddress.End}"
}

variable "kubernetes_cluster_ip_start" {
  default = "#{Kubernetes.Cluster.IPAddress.Start}"
}

variable "kubernetes_cluster_ip_end" {
  default = "#{Kubernetes.Cluster.IPAddress.End}"
}