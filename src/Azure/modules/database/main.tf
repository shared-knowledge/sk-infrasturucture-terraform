
// ======================================================================= Resources Group  ================================================================================== //

resource "azurerm_resource_group" "dks-database-resource-group" {
    lifecycle {
        prevent_destroy = true
        ignore_changes = ["name", "location"]
    }
    name                          = "dks-database-${var.environment}-tf"
    location                      = "${var.resource_location}"
}

// ======================================================================= Database Resources =============================================================================== //

resource "azurerm_mysql_server" "dks-mysql-server" {
    lifecycle {
        prevent_destroy = true
        ignore_changes = ["name", "resource_group_name", "location", "version", "administrator_login", "sku"]
    }
    name                          = "dks-my-sql-server-${var.environment}"
    resource_group_name           = "${azurerm_resource_group.dks-database-resource-group.name}"
    location                      = "${azurerm_resource_group.dks-database-resource-group.location}"

    sku {
        name                      = "B_Gen5_1"
        capacity                  = 1
        tier                      = "Basic"
        family                    = "Gen5"
    }

    storage_profile {
        storage_mb            = 5120
        backup_retention_days = 7
    }

    administrator_login          = "dksmysqladmin"
    administrator_login_password = "${var.sql_server_admin_password}"
    version                      = "5.7"
    ssl_enforcement              = "Enabled"

    tags = {
      Environment                 = "${var.environment}"
    }
}

resource "azurerm_mysql_database" "dks-mysql-db" {
    lifecycle {
        prevent_destroy = true
        ignore_changes = ["name", "resource_group_name", "server_name", "charset", "collation"]
    }
    name                = "dks-my-sql-db-${var.environment}"
    resource_group_name = "${azurerm_resource_group.dks-database-resource-group.name}"
    server_name         = "${azurerm_mysql_server.dks-mysql-server.name}"
    charset             = "utf8"
    collation           = "utf8_unicode_ci"
}

// ================================================================ Firewall Rules for SQL Server =========================================================================== //

resource "azurerm_mysql_firewall_rule" "dks-mysql-server-firewall-k8s-cluster" {
  name                = "KubernetesCluster"
  resource_group_name = "${azurerm_resource_group.dks-database-resource-group.name}"
  server_name         = "${azurerm_mysql_server.dks-mysql-server.name}"
  start_ip_address    = "${var.kubernetes_cluster_ip_start}"
  end_ip_address      = "${var.kubernetes_cluster_ip_end}"
}

resource "azurerm_mysql_firewall_rule" "dks-mysql-server-firewall-octopus-jump-box" {
  name                = "OctopusJumpBox"
  resource_group_name = "${azurerm_resource_group.dks-database-resource-group.name}"
  server_name         = "${azurerm_mysql_server.dks-mysql-server.name}"
  start_ip_address    = "${var.octopus_jumpbox_ip_start}"
  end_ip_address      = "${var.octopus_jumpbox_ip_end}"
}
