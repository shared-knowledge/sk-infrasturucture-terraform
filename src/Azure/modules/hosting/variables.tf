
## =================================  !!!!!!!!!!!!!!!!!!! DO NOT CHANGE VARIABLES  !!!!!!!!!!!!!!!!!!!!!!!!!! ========================== ##

## Changing variables here or on octopus may cause resource deleteion and re-creation. 

## ========================================================== General Variables ======================================================== ##
variable "environment" {
    default = "#{Environment}"
}

variable "resource_location" {
    default = "#{ResourceLocation}"
}

## ===================================================== Kubernetes Cluster Variables ================================================ ##

variable "kubernetes_max_node_count" {
    default = "#{AKS.MaxNodeCount}" 
}

variable "kubernetes_node_count" {
    default = "#{AKS.NodeCount}"
}

variable "service_principle_client_id" {
    default = "#{AKS.ServicePrinciple.ClientId}"
}

variable "service_principle_client_secret" {
    default = "#{AKS.ServicePrinciple.ClientSecret}"
}

variable "kubernetes_version" {
    default = "#{AKS.KubernetesVersion}" 
}

variable "kubernetes_ingress_ip_name" {
    default = "#{AKS.Ingress.IpName}" 
}

variable "kubernetes_ingress_dns" {
    default = "#{AKS.Ingress.DnsName}" 
}