resource "azurerm_resource_group" "dks-hosting-resource-group" {
    lifecycle {
        prevent_destroy = true
        ignore_changes = ["name", "location"]
    }
    name                          = "dks-hosting-${var.environment}-tf"
    location                      = "${var.resource_location}"
}

resource "azurerm_kubernetes_cluster" "dks-aks-k8s-cluster" {
    lifecycle {
        prevent_destroy = true
        ignore_changes = ["name", "location", "resource_group_name", "dns_prefix", "node_resource_group", "role_based_access_control", "agent_pool_profile"]
    }

    name                        = "dks-aks-k8s-cluster-${var.environment}"
    location                    = "${azurerm_resource_group.dks-hosting-resource-group.location}"
    resource_group_name         = "${azurerm_resource_group.dks-hosting-resource-group.name}"
    dns_prefix                  = "aks-k8s-${var.environment}"
    kubernetes_version          = "${var.kubernetes_version}"

    agent_pool_profile {
        name                    = "akspool${var.environment}"
        vm_size                 = "Standard B2s"
        count                   = "${var.kubernetes_node_count}"
        type                    = "VirtualMachineScaleSets"
        enable_auto_scaling     = true
        min_count               = 1
        max_count               = "${var.kubernetes_max_node_count}"
        os_type                 = "Linux"
    }

    service_principal {
        client_id               = "${var.service_principle_client_id}"
        client_secret           = "${var.service_principle_client_secret}"
    }

    tags = {
        Environment             = "${var.environment}"
    }
}

resource "azurerm_public_ip" "dks-aks-k8s-cluster-ip-dns-name" {    
    lifecycle {
        prevent_destroy = true
        ignore_changes = ["name", "location", "resource_group_name", "domain_name_label"]
    }

    name                        = "${var.kubernetes_ingress_ip_name}"
    location                    = "${azurerm_resource_group.dks-hosting-resource-group.location}"
    resource_group_name         = "${azurerm_kubernetes_cluster.dks-aks-k8s-cluster.node_resource_group}"
    allocation_method           = "Static"
    domain_name_label           = "${var.kubernetes_ingress_dns}"

    tags = {
        Environment             = "${var.environment}"
    }
}