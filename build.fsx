#r "paket: groupref fake //"
#load "./.fake/build.fsx/intellisense.fsx"

open Fake
open Fake.Core
open Fake.DotNet
open Fake.IO
open Fake.Tools
open System
open Fake.IO.FileSystemOperators
open Fake.IO.Globbing.Operators

let octopusUrl         = "<INFRASTRUCTURE_OCTOPUS_URL>"
let octopusApiKey      = "<INFRASTRUCTURE_OCTOPUS_APIKEY>"

let nupkgsDir          = "nupkgs"
let gitVersion         = GitVersion.generateProperties id
let currentDirectory   = IO.Directory.GetCurrentDirectory ()
let octoPath           = Globbing.Tools.findToolInSubPath "dotnet-octo.dll" "packages/build/Octopus.DotNet.Cli/tools/netcoreapp2.1/any"

Target.create "Clean" <| fun _ ->
    [nupkgsDir]
    |> Shell.cleanDirs

Target.create "PaketPack" <| fun _ ->
    IO.Directory.ensure nupkgsDir
    Paket.pack <| fun p ->
        { p with
            OutputPath = nupkgsDir
            Version = gitVersion.SemVer }

Target.create "PackagePush" <| fun _ ->
    let packages = !! (nupkgsDir </> "*.nupkg")
    if Seq.isEmpty packages then failwithf "%s contains no packages to push" nupkgsDir
    
    let rec push trial package =
        let args =
            [ octoPath 
              "push"
              sprintf "--package=%s" package
              sprintf "--server=%s" octopusUrl
              sprintf "--apiKey=%s" octopusApiKey ]

        let success =
            CreateProcess.fromRawCommand "dotnet" args
            |> Proc.run
            |> fun r -> r.ExitCode = 0

        if not success then
            Trace.logfn "Attempt %i to push package %s failed" trial package
            if trial >= 5 then failwithf "Failed to push package %s after %i retries" package trial
            else package |> push (trial + 1)

    packages |> Seq.iter (push 1)

Target.create "OctopusRelease" <| fun _ ->
    let tagOk,tagLines,_ = Git.CommandHelper.runGitCommand currentDirectory "describe origin/master --tags --abbrev=0"
    let histRange =
        match tagOk with
        | true -> sprintf "%s..%s" (tagLines |> Seq.head) "HEAD"
        | _ -> "HEAD"

    let _,historyLines,histError = Git.CommandHelper.runGitCommand currentDirectory <| sprintf "log %s --pretty=format:\"- **%%h** %%s\"" histRange
    if histError <> "" then failwithf "Failed to get change history for release notes: %s" histError

    IO.File.writeNew "ReleaseNotes.md" historyLines

    let args =
        [ octoPath
          "create-release"
          "--project"; "Azure Infrastructure"
          "--package"; sprintf "Plan Terraform Apply:%s" gitVersion.SemVer
          "--package"; sprintf "Apply Terraform Template:%s" gitVersion.SemVer
          "--releasenotesfile"; "ReleaseNotes.md"
          "--server"; octopusUrl
          "--apikey"; octopusApiKey ]

    CreateProcess.fromRawCommand "dotnet" args
    |> CreateProcess.ensureExitCodeWithMessage (sprintf "Failed to create release with version %s" gitVersion.SemVer)
    |> Proc.run
    |> ignore

Target.create "Publish" ignore

open Fake.Core.TargetOperators

"Clean"
  ==> "PaketPack"
  ==> "PackagePush"
  ==> "OctopusRelease"

Target.runOrDefaultWithArguments "OctopusRelease"